<?php

namespace Drupal\kic_helpdesk\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Image' block.
 *
 * @Block(
 *   id = "kic_helpdesk_helpdesk_block",
 *   admin_label = @Translation("KIC Helpdesk"),
 *   category = @Translation("Help"),
 * )
 */
class HelpdeskBlock extends BlockBase implements BlockPluginInterface {

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    $config = $this->getConfiguration();
    $element = [[

      '#type' => 'container',
      '#attributes' => [
        'id' => 'helpdesk',
      ],
      '#theme' => 'kic_helpdesk',
      '#form' => \Drupal::formBuilder()->getForm('Drupal\kic_helpdesk\Form\HelpdeskForm'),
      '#description' => $this->t('Please describe your technical problem using the textbox below.'),
      '#hint' => $this->t('<b>Hint:</b> Have you already checked <a href="/faq" target="_blank" rel="noopener">our FAQ</a> for an answer to your question?'),
      '#attached' => [
        'library' => ['kic_helpdesk/helpdesk'],
      ],
    ],
    ];

    return $element;
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param mixed $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param mixed $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\Core\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\Core\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

  }

}
