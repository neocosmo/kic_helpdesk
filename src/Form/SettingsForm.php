<?php

namespace Drupal\kic_helpdesk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration settings form for kic_helpdesk.
 */
class SettingsForm extends ConfigFormBase {
  const SETTINGS = 'kic_helpdesk.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    // Unique ID of the form.
    return 'kic_helpdesk_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config(static::SETTINGS);

    $form['mail_address'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('helpdesk_mail_address'),
      '#title' => $this
        ->t('Mail address for helpdesk issues'),
    ];

    $form['chatbot_activation'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('chatbot_activation'),
      '#title' => $this
        ->t('Activate Chatbot'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $config->set('helpdesk_mail_address', $form_state->getValue('mail_address'));
    $config->set('chatbot_activation', $form_state->getValue('chatbot_activation'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

}
