<?php

namespace Drupal\kic_helpdesk\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for messages in the helpdesk panel.
 */
class HelpdeskForm extends FormBase {
  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  protected $mailManager;

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $account;

  /**
   * Email Validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructor of HelpdeskForm.
   *
   * @param \Drupal\Core\Mail\MailManager $mailManager
   *   The mail manager.
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The user account.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The Email Validator Service.
   */
  public function __construct(
    MailManager $mailManager,
    AccountProxy $account,
    EmailValidatorInterface $email_validator) {

    $this->mailManager = $mailManager;
    $this->account = $account;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.mail'), $container->get('current_user'), $container->get('email.validator'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    // Unique ID of the form.
    return 'helpdesk_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('honeypot')) {
      \Drupal::service('honeypot')->addFormProtection($form, $form_state,
      ['honeypot', 'time_restriction']);
    }
    // Create a $form API array.
    $form['#attributes'] = [
      'id' => 'helpdesk-form',
      'class' => ['autoValidate'],
    ];
    $form['report_title'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this
        ->t('Title of your issue'),
      '#attributes' => [
        'id' => 'issuetitle',
        'class' => ['control-label'],
        'placeholder' => $this
          ->t('Title of your issue'),
      ],
    ];
    $form['category'] = [
      '#type' => 'select',
      '#options' => [
        'technical' => $this
          ->t('Technical question'),
        'content' => $this
          ->t('Content question'),
        'coop' => $this
          ->t('Cooperation question'),
      ],
      '#title' => $this
        ->t('Category'),
      '#attributes' => [
        'id' => 'category',
        'class' => ['form-control'],
      ],
    ];
    $form['issuereport'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this
        ->t('Your Issue'),
      '#attributes' => [
        'id' => 'issuereport',
        'class' => ['control-label'],
      ],
      '#cols' => '50',
      '#rows' => '6',
      '#required' => TRUE,
    ];
    if (!$this->account->getAccountName()) {
      $form['name'] = [
        '#type' => 'textfield',
        '#required' => FALSE,
        '#title' => $this
          ->t('Your name'),
        '#attributes' => [
          'id' => 'name',
          'class' => ['control-label'],
          'placeholder' => $this
            ->t('Your name'),
        ],
      ];
    }
    if (!$this->account->getEmail()) {
      $form['mail'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this
          ->t('Your mail address'),
        '#attributes' => [
          'id' => 'mail',
          'class' => ['control-label'],
          'placeholder' => $this
            ->t('Your mail address'),
        ],
      ];
    }
    $form['captcha'] = [
      '#type' => 'captcha',
      '#captcha_type' => 'default',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['buttons'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'mb30',
          'mt20',
          'buttons',
        ],
      ],
    ];
    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#value' => $this
        ->t('Report issue'),
      '#attributes' => [
        'id' => 'issue-submit-button',
        'class' => [
          'btn',
          'btn-primary',
          'mr20',
          'helpdesk-brand',
        ],
      ],
    ];
    $form['buttons']['cancel'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this
        ->t('Cancel'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-default',
          'helpdesk-widget-only',
        ],
        'id' => 'issue-cancel-button',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate submitted form data.
    if ($form_state->getValue('mail') && !$this->emailValidator->isValid($form_state->getValue('mail'))) {
      $form_state->setErrorByName('mail', $this->t('Please enter a valid email address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('kic_helpdesk.settings');
    $mailaddress = $config->get('helpdesk_mail_address');
    $newMail = $this->mailManager;
    $params['user'] = !empty($this->account->getAccountName()) ? $this->account->getAccountName() : $form_state->getValue('name');
    if (empty($params['user'])) {
      $params['user'] = "Anonymous";
    }
    $params['usermail'] = $this->account->getEmail() ?? $form_state->getValue('mail');
    $params['topic'] = $form['category']['#options'][$form_state->getValue('category')];
    $params['title'] = $form_state->getValue('report_title');
    $params['url'] = \Drupal::request()->getRequestUri();
    $params['browser'] = $_SERVER['HTTP_USER_AGENT'];
    $params['issue'] = $form_state->getValue('issuereport');
    $result = $newMail->mail('kic_helpdesk', 'helpdeskMail', $mailaddress, 'en', $params, $reply = NULL, $send = TRUE);
    if ($result['result'] === TRUE) {
      \Drupal::messenger()->addMessage($this->t('Mail has been sent.'));
    }
  }

}
