(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.kic_helpdesk = {
    attach: function (context, settings) {
      $('#helpdesk-button',context).show();

      $('#helpdesk-button', context).on('click' , function() {
        if (Drupal.kic_helpdesk.helpDeskTabOpen == false) {
          Drupal.kic_helpdesk.open();
       } else {
        Drupal.kic_helpdesk.close();
       }
     });

      $('.helpdesk-container', context).on('click', '#issue-cancel-button', function() {
        Drupal.kic_helpdesk.close();
        document.getElementById('helpdesk-form').reset();
        return false;
      });

      $('.helpdesk-container', context).on('click', '#issue-back-button', function() {
        $('#helpdesk-panel .helpdesk-error-box').hide();
        $('#helpdesk-panel .helpdesk-default-box').fadeIn('medium');
      });



    }
  };
  Drupal.kic_helpdesk={
    helpdeskSession: null,
    w: 0,
    helpdeskStatus: 'default',
    helpDeskTabOpen: false,
    chatbotLoading: false,


    open: function () {
      console.log('open');

      Drupal.kic_helpdesk.helpdeskSession = Math.random().toString(36).substr(2, 12);



    var browserLanguage;
    var isIE = navigator.userAgent.toLowerCase().indexOf('msie') != -1;
    if(isIE) {
      browserLanguage = navigator.browserLanguage;
    } else {
      browserLanguage = navigator.language;
    }

    $('.dimmer').css('opacity', 0).show().animate({
      'opacity' : 0.75
    },{
      duration: 200
    });
    $('.dimmer').on ('click', function(){
      $('#issue-cancel-button').trigger('click');
    });
    //load
    //$('#helpdesk-ajax-container').load('/helpdesk', function () {
      $('#helpdesk-panel .helpdesk-default-box').show();
      $('#helpdesk-panel').animate({
        width: 'show',
        opacity: 'show'
      }, {
        duration: 200,
        step: function () {
          Drupal.kic_helpdesk.w = $('#helpdesk-panel').outerWidth();
          $('#helpdesk-button').css("right", Drupal.kic_helpdesk.w - 11);
          $('#helpdesk-panel .helpdesk-success-box').hide();
          $('#helpdesk-panel .helpdesk-error-box').hide();
        },
        complete: function () {
          Drupal.kic_helpdesk.helpDeskTabOpen = true;
          if ($('#course_id_container').text()){
            $('select#category').val($('#course_id_container').text()).change();
          }

        }
      });



      /*$('#helpdesk-form').submit(function () {
        var issueReport = $('#issuereport').val(),
          issueTitle = $('#issuetitle').val(),
          category = $('#category').val(),
          mail = null,
          topic = $("input[name='question_topic']:checked").val(),
          url = document.URL,
          userAgent = navigator.userAgent,
          language = browserLanguage,
          cookieEnabled = navigator.cookieEnabled;
        try {
          mail = $('#issueemail').val();
        } catch (err) {
        }
        $.ajax({
          type: 'POST',
          url: 'https://learn.ki-campus.org/helpdesk/',
          dataType: 'json',
          data: {
            'report': issueReport,
            'url': url,
            'topic': topic,
            'userAgent': userAgent,
            'language': language,
            'category': category,
            'cookie': cookieEnabled,
            'title': issueTitle,
            'mail': mail
          },
          success: function (data) {
            if (data.success === false) {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#helpdesk-panel .helpdesk-error-box').fadeIn('slow', function () {
                  Drupal.kic_helpdesk.helpdeskStatus = 'error';
                });
              });
            } else {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#issuereport').val("");
                $('#issuetitle').val("");
                $('#helpdesk-panel .helpdesk-success-box').fadeIn('slow', function () {
                  Drupal.kic_helpdesk.helpdeskStatus = 'success';
                });
              });

            }
          }
        });
        return false;

      });*/
    },

    close: function () {
      console.log('close');
      $('#helpdesk-panel').animate({
        width: 'hide',
        opacity: 'hide'
      }, {
        duration: 200,
        step: function(now, fx) {
          Drupal.kic_helpdesk.w = $('#helpdesk-panel').width();
          $('#helpdesk-button').css('right', Drupal.kic_helpdesk.w - 1);
        },
        complete: function () {
          if (Drupal.kic_helpdesk.helpdeskStatus === 'default') { // helpdesk was closed without submitting a ticket
            $('#helpdesk-panel .helpdesk-default-box').fadeOut('fast');
            $('#helpdesk-panel .helpdesk-success-box').hide();
            $('#helpdesk-panel .helpdesk-error-box').hide();
          } else if (Drupal.kic_helpdesk.helpdeskStatus === 'success') { // helpdesk was closed and a ticket was successfully submitted
            $('#helpdesk-panel .helpdesk-success-box').fadeOut('fast', function () {
              $('#helpdesk-panel .helpdesk-default-box').hide();
              $('#helpdesk-panel .helpdesk-error-box').hide();
              Drupal.kic_helpdesk.helpdeskStatus = 'default';
            });
          } else if (Drupal.kic_helpdesk.helpdeskStatus === 'error') { // helpdesk was closed but the ticket was not successfully submitted
            $('#helpdesk-panel .helpdesk-error-box').fadeOut('fast', function () {
              $('#helpdesk-panel .helpdesk-default-box').hide();
              $('#helpdesk-panel .helpdesk-success-box').hide();
              Drupal.kic_helpdesk.helpdeskStatus = 'default';
            });
          }
        }
      });

      $('.dimmer').animate({
        'opacity' : 0
      },{
        duration: 200,
        complete: function() {
          $(this).hide();
        }
      });
      $('.dimmer').on('click', function() {});
      Drupal.kic_helpdesk.helpDeskTabOpen = false;
      Drupal.kic_helpdesk.helpdeskSession = null;


    }


  };

})(jQuery, Drupal);
