(function ($, Drupal) {
  'use strict';

  function Chatbot(element) {
    this.conversation$ = element;
    this.$input = $('#chatbot-typing-area');
    this.$sendButton = $('#chatbot-enter-btn');
    this.loading = false;

    const isRestored = this.restoreMessages(); // Restore messages from localStorage
    if (!isRestored) {
      this.greet(); // Only show greeting if no valid history exists
    }

    this.listen();
    this.$input.focus();
  }

  Chatbot.prototype = {
    listen: function () {
      var bot = this;
      this.$sendButton.on('click', function (e) {
        e.preventDefault();
        bot.handleInput();
      });
      this.$input.on('keypress', function (e) {
        if (e.which === 13) {
          e.preventDefault();
          bot.handleInput();
        }
      });
    },

    handleInput: function (payload) {
      var startTime = new Date().getTime();
      var question = this.$input.val().trim();
      if (!question) return; // Prevent sending empty messages

      var url = "https://kic-restapi-prod.azurewebsites.net/api/chat";
      var token = "drupal-tZGiw_a.r)6qQ6A7Yq8cUob7$T!8A68b";
      var messages = this.getMessagesFromStorage();

      this.$input.val('');
      this.addUtterance(question, 'chatbot-question');
      this.startLoadingMessage();

      var bot = this;
      if (payload) question = payload;
      messages.push({ "content": question, "role": 'user' });
      this.saveMessagesToStorage(messages);

      // API Request
      $.ajax({
        type: 'POST',
        url: url,
        contentType: 'application/json',
        headers: { "Api-Key": token },
        dataType: 'json',
        data: JSON.stringify({ 'messages': messages, 'model': 'Llama3' }),
        complete: function (xhr, status) {
          var requestTime = new Date().getTime() - startTime;

          if (status === 'success') {
            var response = $.parseJSON(xhr.responseText);
            Drupal.kic_helpdesk.enforceMinimumDelay(1000, requestTime, function () {
              bot.handleResponse(response);
            });
          } else {
            bot.stopLoadingMessage();
          }
          bot.$input.focus();
          bot.conversation$.scrollTop = bot.conversation$.scrollHeight;
        },
      });
    },

    handleResponse: function (response) {
      this.stopLoadingMessage();
      if (!response || !response.message) return;
      this.addUtterance(response.message, 'chatbot-answer');

      var messages = this.getMessagesFromStorage();
      messages.push({ "content": response.message, "role": 'assistant' });
      this.saveMessagesToStorage(messages);
    },

    startLoadingMessage: function () {
      if (!this.loadingMessage) {
        this.loadingMessage = document.createElement('div');
        this.loadingMessage.classList.add('chatbot-message-box', 'chatbot-answer');
        this.loadingMessage.innerHTML = `
          <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
          </div>
        `;
        this.conversation$.insertBefore(this.loadingMessage, document.getElementById('utterance--placeholder'));
        this.conversation$.scrollTop = this.conversation$.scrollHeight;
      }
    },

    stopLoadingMessage: function () {
      if (this.loadingMessage) {
        this.conversation$.removeChild(this.loadingMessage);
        this.loadingMessage = null;
      }
      this.loading = false;
    },

    greet: function () {
      var greeting = Drupal.t('Welcome to the AI-Campus! How can I help you?');
      this.addUtterance(greeting, 'chatbot-answer');
      this.saveMessagesToStorage([{ content: greeting, role: 'assistant' }]); // Start with greeting in history
    },

    addUtterance: function (utterance, type) {
      var message = document.createElement('div');
      message.classList.add('chatbot-message-box', type);
      message.innerHTML = utterance.trim();
      this.conversation$.insertBefore(message, document.getElementById('utterance--placeholder'));
      this.conversation$.scrollTop = this.conversation$.scrollHeight;
    },

    saveMessagesToStorage: function (messages) {
      var chatData = {
        messages: messages,
        timestamp: new Date().getTime()
      };
      localStorage.setItem('chatbotMessages', JSON.stringify(chatData));
    },

    getMessagesFromStorage: function () {
      var chatData = localStorage.getItem('chatbotMessages');
      if (chatData) {
        chatData = JSON.parse(chatData);
        var currentTime = new Date().getTime();
        if (currentTime - chatData.timestamp <= 15 * 60 * 1000) {
          return chatData.messages || [];
        }
        localStorage.removeItem('chatbotMessages'); // Clear expired messages
      }
      return [];
    },

    restoreMessages: function () {
      var messages = this.getMessagesFromStorage();
      if (messages.length) {
        this.clearConversation(); // Clear any preloaded conversation to prevent duplication
        messages.forEach((message) => {
          var type = message.role === 'user' ? 'chatbot-question' : 'chatbot-answer';
          this.addUtterance(message.content, type);
        });
        return true; // Indicates messages were restored
      }
      return false; // No messages restored
    },

    clearConversation: function () {
      while (this.conversation$.firstChild && this.conversation$.firstChild.id !== 'utterance--placeholder') {
        this.conversation$.removeChild(this.conversation$.firstChild);
      }
    }
  };

  Drupal.behaviors.kic_helpdesk = {
    attach: function (context, settings) {
      $('#helpdesk', context).once('myCustomOnceId').each(function (index) {
        $('#helpdesk-button', context).show();

        // Restore chat window state
        if (localStorage.getItem('chatWindowState') === 'open') {
          Drupal.kic_helpdesk.open();
        }

        // Set first tab active by default
        $('#helpdesk-contact-tabs li:first-child').addClass('active');
        $('#helpdesk-contact-tabs li:first-child a.tab').addClass('active');

        $('#helpdesk-button', context).on('click', function () {
          if (!Drupal.kic_helpdesk.helpDeskTabOpen) {
            Drupal.kic_helpdesk.open();
          } else {
            Drupal.kic_helpdesk.close();
          }
        });

        $('.helpdesk-container', context).on('click', '#issue-cancel-button', function () {
          Drupal.kic_helpdesk.close();
          document.getElementById('helpdesk-form').reset();
          return false;
        });

        var conversation = document.getElementById('chatbot-conversation');
        if (conversation) {
          new Chatbot(conversation);
        }
      });
    }
  };

  Drupal.kic_helpdesk = {
    helpdeskSession: null,
    prototypeOn: null,
    helpdeskStatus: 'default',
    helpDeskTabOpen: false,

    enforceMinimumDelay: function (minDelay, timePassed, callback) {
      var delay = Math.max(0, minDelay - timePassed);
      setTimeout(callback, delay);
    },

    open: function () {
      $('#helpdesk-panel').css({
        display: 'block',
        bottom: '-100%',
        right: '10px'
      }).animate({
        bottom: '90px'
      }, 200);

      Drupal.kic_helpdesk.helpDeskTabOpen = true;

      // Save window state as open
      localStorage.setItem('chatWindowState', 'open');

      if (!Drupal.kic_helpdesk.prototypeOn) {
        var conversation = document.getElementById('chatbot-conversation');
        if (conversation) {
          new Chatbot(conversation);
        }
        Drupal.kic_helpdesk.prototypeOn = true;
      }
    },

    close: function () {
      $('#helpdesk-panel').animate({
        bottom: '-100%'
      }, 200, function () {
        $(this).css({ display: 'none' });
      });

      Drupal.kic_helpdesk.helpDeskTabOpen = false;

      // Save window state as closed
      localStorage.setItem('chatWindowState', 'closed');
    }
  };
})(jQuery, Drupal);
