jQuery.ready(function() {
  var helpdeskSession = null;

  var track = function(verb, context) {
    // We currently cannot track events for guest users
    if (!gon.user_id) return;

    context['helpdesk_session_id'] = helpdeskSession;
    context['chatbot_version'] = getCurrentFeature();
    context['language'] = I18n.locale;

    $(document).trigger('track-event', {
      verb: verb,
      resource: window.location.pathname,
      resourceType: 'page',
      inContext: context
    });
  };

  // Set up the chatbot variant 1, if needed
  var prototype1 = function() {
    var $suggestions = $('#helpdesk-suggestions');
    if ($suggestions.length === 0) return;

    var $form = $('#helpdesk-form');
    var $title = $form.find('[name=issuetitle]');

    var suggest = function () {
      $suggestions.removeClass('is-loaded');

      var start_time = new Date().getTime();

      var question = $title.val();
      if (question.trim() === '') return;

      $suggestions.addClass('is-loading');
      $.ajax({
        type: 'POST',
        url: 'https://chatbot.xopic.de/webhooks/rest/webhook',
        dataType: 'json',
        data: JSON.stringify({message: question}),
        success: function (response) {
          if (response.length > 0) {
            $suggestions.addClass('is-loaded');
            $suggestions.find('.result-text p').html(response[0].text);
          }
        },
        complete: function (data, status) {
          $suggestions.removeClass('is-loading');
          var requestTime = new Date().getTime() - start_time;
          if (status === 'success') {
            var response = data.responseJSON.length > 0 ? data.responseJSON[0].text : '';
            track('helpdesk_chatbot_replied', {
              question: question,
              response: response,
              request_time: requestTime,
              status: status
            });
          } else {
            track('helpdesk_chatbot_replied', {
              question: question,
              request_time: requestTime,
              status: status
            });
          }
        }
      });
    };

    $title.on('blur', suggest);
  };

  var w = 0,
    helpdeskStatus = 'default',
    helpDeskTabOpen = false,
    chatbotLoadingInterval,
    chatbotLoading = false;

  window.openHelpdeskLayer = function() {
    // Generate a new "session" identifier so that we can relate tracking events to each other
    helpdeskSession = Math.random().toString(36).substr(2, 12);

    track('helpdesk_opened', {});

    var browserLanguage;
    var isIE = navigator.userAgent.toLowerCase().indexOf('msie') != -1;
    if(isIE) {
      browserLanguage = navigator.browserLanguage;
    } else {
      browserLanguage = navigator.language;
    }

    $('.dimmer').css('opacity', 0).show().animate({
      'opacity' : 0.75
    },{
      duration: 200
    });
    $('.dimmer').on ('click', function(){
      $('#issue-cancel-button').trigger('click');
    });
    //load
    //$('#helpdesk-ajax-container').load('/helpdesk', function () {
      $('#helpdesk-panel .helpdesk-default-box').show();
      $('#helpdesk-panel').animate({
        width: 'show',
        opacity: 'show'
      }, {
        duration: 200,
        step: function () {
          w = $('#helpdesk-panel').outerWidth();
          $('#helpdesk-button').css("right", w - 11);
          $('#helpdesk-panel .helpdesk-success-box').hide();
          $('#helpdesk-panel .helpdesk-error-box').hide();
        },
        complete: function () {
          helpDeskTabOpen = true;
          if ($('#course_id_container').text()){
            $('select#category').val($('#course_id_container').text()).change();
          }
          getInitialChatbotGreeting();
          prototype1();
        }
      });

      $('#chatbot-control-window').on('keydown', function (e) {
        if (e.which == 13) { // key 13 is the "enter"
          handleChatbotQuestionInput();
        }
      });

      $('#chatbot-enter-btn').click(function () {
        handleChatbotQuestionInput();
      });

      $('#chatbot-typing-area').focus();

      $('#helpdesk-form').submit(function () {
        var issueReport = $('#issuereport').val(),
          issueTitle = $('#issuetitle').val(),
          category = $('#category').val(),
          mail = null,
          topic = $("input[name='question_topic']:checked").val(),
          url = document.URL,
          userAgent = navigator.userAgent,
          language = browserLanguage,
          cookieEnabled = navigator.cookieEnabled;
        try {
          mail = $('#issueemail').val();
        } catch (err) {
        }
        $.ajax({
          type: 'POST',
          url: '/helpdesk/',
          dataType: 'json',
          data: {
            'report': issueReport,
            'url': url,
            'topic': topic,
            'userAgent': userAgent,
            'language': language,
            'category': category,
            'cookie': cookieEnabled,
            'title': issueTitle,
            'mail': mail
          },
          success: function (data) {
            if (data.success === false) {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#helpdesk-panel .helpdesk-error-box').fadeIn('slow', function () {
                  helpdeskStatus = 'error';
                });
              });
            } else {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#issuereport').val("");
                $('#issuetitle').val("");
                $('#helpdesk-panel .helpdesk-success-box').fadeIn('slow', function () {
                  helpdeskStatus = 'success';
                });
              });
              track('helpdesk_ticket_created', {
                report: issueReport,
                topic: category,
                question: issueTitle
              });
            }
          }
        });
        return false;

      });
    //});
  };

  function handleChatbotQuestionInput() {
    var start_time = new Date().getTime();
    var question = $('#chatbot-typing-area').val();
    $('#chatbot-typing-area').val("");
    addUtteranceToConversation(question, 'chatbot-question');
    addLoadingState();
    chatbotLoading = true;
    $.ajax({
      type: 'POST',
      url: 'https://chatbot-v2.xopic.de/webhooks/rest/webhook',
      dataType: 'json',
      data: JSON.stringify({
        message: question
      }),
      success: function (response, status) {
        var requestTime = new Date().getTime() - start_time;
        var delayInMilliseconds = requestTime < 200 ? 1000 : 0;
        setTimeout(function () {
          clearLoadingState();
          chatbotLoading = false;
          if (response.length > 0) {
            response.forEach((answer) => {
              addUtteranceToConversation(answer.text, 'chatbot-answer');
            })
          }
          }, delayInMilliseconds);
        track('helpdesk_chatbot_replied', {
          question: question,
          response: response,
          request_time: requestTime,
          status: status
        });
      },
      complete: function (data, status) {
        if (status != 'success' && chatbotLoading) {
          clearLoadingState();
          chatbotLoading = false;
        }
        $('#chatbot-typing-area').focus();
        var chatbotWindow = $('#chatbot-conversation')[0];
        chatbotWindow.scrollTop = chatbotWindow.scrollHeight;
        var requestTime = new Date().getTime() - start_time;
        if (status != 'success') {
          track('helpdesk_chatbot_replied', {
            question: question,
            request_time: requestTime,
            status: status
          });
        }
      }
    });
  }

  function addLoadingState() {
    var loadingMessage = document.createElement('div');
    loadingMessage.setAttribute('id', 'chatbot-loading-dots');
    loadingMessage.classList.add('chatbot-message-box', 'chatbot-answer');
    loadingMessage.textContent = '.';
    $('#chatbot-conversation')[0].appendChild(loadingMessage);
    chatbotLoadingInterval = setInterval(function () {
      loadingMessage.textContent.length >= 3 ? loadingMessage.textContent = '.' : loadingMessage.textContent += '.';
    }, 300);
  }

  function clearLoadingState() {
    $('#chatbot-loading-dots').remove();
    clearInterval(chatbotLoadingInterval);
  }

  function getInitialChatbotGreeting() {
    $.ajax({
      type: 'POST',
      url: `https://chatbot-v2.xopic.de/conversations/${helpdeskSession}/trigger_intent`,
      dataType: 'json',
      data: JSON.stringify({name: 'greetUser'}),
      success: function (response) {
        var initialGreetig = response.messages[0].text;
        addUtteranceToConversation(initialGreetig, 'chatbot-answer');
      }
    });
  }

  function addUtteranceToConversation(utterance, type) {
    var chatMessage = document.createElement('div');
    chatMessage.classList.add('chatbot-message-box', type);
    chatMessage.textContent = utterance.trim();
    $('#chatbot-conversation')[0].appendChild(chatMessage);
  }

  function getCurrentFeature() {
    return $('#chatbot-current-feature').length ?
      $('#chatbot-current-feature')[0].dataset.feature : 'default';
  }

  function closeHelpdeskLayer() {
    track('helpdesk_closed', {});

    $('#helpdesk-panel').animate({
      width: 'hide',
      opacity: 'hide'
    }, {
      duration: 200,
      step: function(now, fx) {
        w = $('#helpdesk-panel').width();
        $('#helpdesk-button').css('right', w - 1);
      },
      complete: function () {
        if (helpdeskStatus === 'default') { // helpdesk was closed without submitting a ticket
          $('#helpdesk-panel .helpdesk-default-box').fadeOut('fast');
          $('#helpdesk-panel .helpdesk-success-box').hide();
          $('#helpdesk-panel .helpdesk-error-box').hide();
        } else if (helpdeskStatus === 'success') { // helpdesk was closed and a ticket was successfully submitted
          $('#helpdesk-panel .helpdesk-success-box').fadeOut('fast', function () {
            $('#helpdesk-panel .helpdesk-default-box').hide();
            $('#helpdesk-panel .helpdesk-error-box').hide();
            helpdeskStatus = 'default';
          });
        } else if (helpdeskStatus === 'error') { // helpdesk was closed but the ticket was not successfully submitted
          $('#helpdesk-panel .helpdesk-error-box').fadeOut('fast', function () {
            $('#helpdesk-panel .helpdesk-default-box').hide();
            $('#helpdesk-panel .helpdesk-success-box').hide();
            helpdeskStatus = 'default';
          });
        }
      }
    });

    $('.dimmer').animate({
      'opacity' : 0
    },{
      duration: 200,
      complete: function() {
        $(this).hide();
      }
    });
    $('.dimmer').on('click', function() {});
    helpDeskTabOpen = false;
    helpdeskSession = null;
  }

  $('#helpdesk-button').show();

  $('#helpdesk-button').on('click' , function() {
    if (helpDeskTabOpen == false) {
      openHelpdeskLayer();
    } else {
      closeHelpdeskLayer();
    }
  });

  $('.helpdesk-container').on('click', '#issue-cancel-button', function() {
    closeHelpdeskLayer();
    document.getElementById('helpdesk-form').reset();
  });

  $('.helpdesk-container').on('click', '#issue-back-button', function() {
    $('#helpdesk-panel .helpdesk-error-box').hide();
    $('#helpdesk-panel .helpdesk-default-box').fadeIn('medium');
  });
});
