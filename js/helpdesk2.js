(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.kic_helpdesk = {
    attach: function (context, settings) {
      $('#helpdesk-button',context).show();

      $('#helpdesk-button', context).on('click' , function() {
        if (Drupal.kic_helpdesk.helpDeskTabOpen == false) {
          Drupal.kic_helpdesk.open();
       } else {
        Drupal.kic_helpdesk.close();
       }
     });



      $('.helpdesk-container', context).on('click', '#issue-cancel-button', function() {
        Drupal.kic_helpdesk.close();
        document.getElementById('helpdesk-form').reset();
        return false;
      });

      $('.helpdesk-container', context).on('click', '#issue-back-button', function() {
        $('#helpdesk-panel .helpdesk-error-box').hide();
        $('#helpdesk-panel .helpdesk-default-box').fadeIn('medium');
      });

      // $('#chatbot-control-window').on('keydown', function (e) {
      //   if (e.which == 13) { // key 13 is the "enter"
      //     Drupal.kic_helpdesk.handleChatbotQuestionInput();
      //     return false;
      //   }
      // });

      $('#chatbot-enter-btn').click(function () {
        Drupal.kic_helpdesk.handleChatbotQuestionInput();
        return false;
      });

      $('#chatbot-typing-area').focus();


    }
  };
  Drupal.kic_helpdesk={
    helpdeskSession: null,
    w: 0,
    helpdeskStatus: 'default',
    helpDeskTabOpen: false,
    chatbotLoadingInterval: null,
    chatbotLoading: false,
    prototype1: function() {
      var $suggestions = $('#helpdesk-suggestions');
      if ($suggestions.length === 0) return;

      var $form = $('#helpdesk-form');
      var $title = $form.find('[name=issuetitle]');

      var suggest = function () {
        $suggestions.removeClass('is-loaded');

        var start_time = new Date().getTime();

        var question = $title.val();
        if (question.trim() === '') return;

        $suggestions.addClass('is-loading');
        $.ajax({
          type: 'POST',
          url: 'https://chatbot-kicampus.xopic.de/webhooks/rest/webhook',
          dataType: 'json',
          data: JSON.stringify({message: question}),
          success: function (response) {
            if (response.length > 0) {
              $suggestions.addClass('is-loaded');
              $suggestions.find('.result-text p').html(response[0].text);
            }
          },
          complete: function (data, status) {
            $suggestions.removeClass('is-loading');
            var requestTime = new Date().getTime() - start_time;
          }
        });
      };

      $title.on('blur', suggest);
    },


    open: function () {
      console.log('open');

      Drupal.kic_helpdesk.helpdeskSession = Math.random().toString(36).substr(2, 12);



    var browserLanguage;
    var isIE = navigator.userAgent.toLowerCase().indexOf('msie') != -1;
    if(isIE) {
      browserLanguage = navigator.browserLanguage;
    } else {
      browserLanguage = navigator.language;
    }

    $('.dimmer').css('opacity', 0).show().animate({
      'opacity' : 0.75
    },{
      duration: 200
    });
    $('.dimmer').on ('click', function(){
      $('#issue-cancel-button').trigger('click');
    });
    //load
    //$('#helpdesk-ajax-container').load('/helpdesk', function () {
      $('#helpdesk-panel .helpdesk-default-box').show();
      $('#helpdesk-panel').animate({
        width: 'show',
        opacity: 'show'
      }, {
        duration: 200,
        step: function () {
          Drupal.kic_helpdesk.w = $('#helpdesk-panel').outerWidth();
          $('#helpdesk-button').css("right", Drupal.kic_helpdesk.w - 11);
          $('#helpdesk-panel .helpdesk-success-box').hide();
          $('#helpdesk-panel .helpdesk-error-box').hide();
        },
        complete: function () {
          Drupal.kic_helpdesk.helpDeskTabOpen = true;
          if ($('#course_id_container').text()){
            $('select#category').val($('#course_id_container').text()).change();
          }
          // Drupal.kic_helpdesk.getInitialChatbotGreeting();
          Drupal.kic_helpdesk.prototype1();
        }

      });



      /*$('#helpdesk-form').submit(function () {
        var issueReport = $('#issuereport').val(),
          issueTitle = $('#issuetitle').val(),
          category = $('#category').val(),
          mail = null,
          topic = $("input[name='question_topic']:checked").val(),
          url = document.URL,
          userAgent = navigator.userAgent,
          language = browserLanguage,
          cookieEnabled = navigator.cookieEnabled;
        try {
          mail = $('#issueemail').val();
        } catch (err) {
        }
        $.ajax({
          type: 'POST',
          url: 'https://learn.ki-campus.org/helpdesk/',
          dataType: 'json',
          data: {
            'report': issueReport,
            'url': url,
            'topic': topic,
            'userAgent': userAgent,
            'language': language,
            'category': category,
            'cookie': cookieEnabled,
            'title': issueTitle,
            'mail': mail
          },
          success: function (data) {
            if (data.success === false) {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#helpdesk-panel .helpdesk-error-box').fadeIn('slow', function () {
                  Drupal.kic_helpdesk.helpdeskStatus = 'error';
                });
              });
            } else {
              $('#helpdesk-panel .helpdesk-default-box').fadeOut('slow', function () {
                $('#issuereport').val("");
                $('#issuetitle').val("");
                $('#helpdesk-panel .helpdesk-success-box').fadeIn('slow', function () {
                  Drupal.kic_helpdesk.helpdeskStatus = 'success';
                });
              });

            }
          }
        });
        return false;

      });*/
    },

    close: function () {
      console.log('close');
      $('#helpdesk-panel').animate({
        width: 'hide',
        opacity: 'hide'
      }, {
        duration: 200,
        step: function(now, fx) {
          Drupal.kic_helpdesk.w = $('#helpdesk-panel').width();
          $('#helpdesk-button').css('right', Drupal.kic_helpdesk.w - 1);
        },
        complete: function () {
          if (Drupal.kic_helpdesk.helpdeskStatus === 'default') { // helpdesk was closed without submitting a ticket
            $('#helpdesk-panel .helpdesk-default-box').fadeOut('fast');
            $('#helpdesk-panel .helpdesk-success-box').hide();
            $('#helpdesk-panel .helpdesk-error-box').hide();
          } else if (Drupal.kic_helpdesk.helpdeskStatus === 'success') { // helpdesk was closed and a ticket was successfully submitted
            $('#helpdesk-panel .helpdesk-success-box').fadeOut('fast', function () {
              $('#helpdesk-panel .helpdesk-default-box').hide();
              $('#helpdesk-panel .helpdesk-error-box').hide();
              Drupal.kic_helpdesk.helpdeskStatus = 'default';
            });
          } else if (Drupal.kic_helpdesk.helpdeskStatus === 'error') { // helpdesk was closed but the ticket was not successfully submitted
            $('#helpdesk-panel .helpdesk-error-box').fadeOut('fast', function () {
              $('#helpdesk-panel .helpdesk-default-box').hide();
              $('#helpdesk-panel .helpdesk-success-box').hide();
              Drupal.kic_helpdesk.helpdeskStatus = 'default';
            });
          }
        }
      });

      $('.dimmer').animate({
        'opacity' : 0
      },{
        duration: 200,
        complete: function() {
          $(this).hide();
        }
      });
      $('.dimmer').on('click', function() {});
      Drupal.kic_helpdesk.helpDeskTabOpen = false;
      Drupal.kic_helpdesk.helpdeskSession = null;


    },

    handleChatbotQuestionInput: function () {
      var start_time = new Date().getTime();
      var question = $('#chatbot-typing-area').val();
      $('#chatbot-typing-area').val("");
      Drupal.kic_helpdesk.addUtteranceToConversation(question, 'chatbot-question');
      Drupal.kic_helpdesk.addLoadingState();
      Drupal.kic_helpdesk.chatbotLoading = true;
      $.ajax({
        type: 'POST',
        url: 'https://chatbot-kicampus-v2.xopic.de/webhooks/rest/webhook',
        dataType: 'json',
        data: JSON.stringify({
          message: question
        }),
        success: function (response, status) {
          var requestTime = new Date().getTime() - start_time;
          var delayInMilliseconds = requestTime < 200 ? 1000 : 0;
          setTimeout(function () {
            Drupal.kic_helpdesk.clearLoadingState();
            Drupal.kic_helpdesk.chatbotLoading = false;
            if (response.length > 0) {
              response.forEach((answer) => {
                Drupal.kic_helpdesk.addUtteranceToConversation(answer.text, 'chatbot-answer');
              })
            }
            }, delayInMilliseconds);
        },
        complete: function (data, status) {
          if (status != 'success' && Drupal.kic_helpdesk.chatbotLoading) {
            Drupal.kic_helpdesk.clearLoadingState();
            Drupal.kic_helpdesk.chatbotLoading = false;
          }
          $('#chatbot-typing-area').focus();
          var chatbotWindow = $('#chatbot-conversation')[0];
          chatbotWindow.scrollTop = chatbotWindow.scrollHeight;
          var requestTime = new Date().getTime() - start_time;
        }
      });
    },

    addLoadingState: function () {
      var loadingMessage = document.createElement('div');
      loadingMessage.setAttribute('id', 'chatbot-loading-dots');
      loadingMessage.classList.add('chatbot-message-box', 'chatbot-answer');
      loadingMessage.textContent = '.';
      $('#chatbot-conversation')[0].appendChild(loadingMessage);
      Drupal.kic_helpdesk.chatbotLoadingInterval = setInterval(function () {
        loadingMessage.textContent.length >= 3 ? loadingMessage.textContent = '.' : loadingMessage.textContent += '.';
      }, 300);
    },

    clearLoadingState: function () {
      $('#chatbot-loading-dots').remove();
      clearInterval(Drupal.kic_helpdesk.chatbotLoadingInterval);
    },

    // getInitialChatbotGreeting: function () {
    //   $.ajax({
    //     type: 'POST',
    //     url: `https://chatbot-kicampus-v2.xopic.de/conversations/${Drupal.kic_helpdesk.helpdeskSession}/trigger_intent`,
    //     dataType: 'json',
    //     data: JSON.stringify({name: 'greetUser'}),
    //     success: function (response) {
    //       var initialGreetig = response.messages[0].text;
    //       Drupal.kic_helpdesk.addUtteranceToConversation(initialGreetig, 'chatbot-answer');
    //     }
    //   });
    // },

    addUtteranceToConversation: function (utterance, type) {
      var chatMessage = document.createElement('div');
      chatMessage.classList.add('chatbot-message-box', type);
      chatMessage.textContent = utterance.trim();
      $('#chatbot-conversation')[0].appendChild(chatMessage);
    },

    getCurrentFeature: function () {
      return $('#chatbot-current-feature').length ?
        $('#chatbot-current-feature')[0].dataset.feature : 'default';
    }



  };

})(jQuery, Drupal);
